
import './assets/css/App.css';
import Header from './components/Header';
import Home from './components/Home';


function App() {
  return (
    <div className="App">
      <Header />
      <Home />

      {/* header */}
        {/* header_top */}
        {/* header_bottom */}

      {/* carousel */}
          {/* carousel_top */}
          {/* carousel_bottom (cards) */}
        
        {/* products */}
    </div>
  );
}

export default App;
