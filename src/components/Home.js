import React from 'react';
import '../assets/css/Home.css'
import Product from './../components/Product';


function Home() {
    return (
        <div className="home">
            <div className="home__container">
                <img className="home__image" src="https://images-eu.ssl-images-amazon.com/images/G/08/digital/video/gateway/placement/launch/connectes/CNCTE_2020_GWBleedingHero_FT_COVIDUPDATE_XSite_1500X600_PV_fr-FR._CB416887570_.jpg" alt=""/>
            </div>

            <div className="home__row">
                <Product />
                <Product />
            </div>   

            <div className="home__row">
                <Product />
                <Product />
                <Product />                
            </div>   

            <div className="home__row">
                <Product />

            </div>            
        </div>
    )
}

export default Home
