import React from 'react'
import '../assets/css/Header_bottom.css';
import RoomOutlinedIcon from '@material-ui/icons/RoomOutlined';

function Header_bottom() {
    return (
        <div className="header_bottom">
            <div className="header_bottom-adress">
                <button className="header_bottom-adressButton">
                    <RoomOutlinedIcon style={{color:"white"}} />
                    <div className="header_bottom-adressText"> 
                        <div className="header_bottom-adressText--Bonjour">Bonjour</div>                      
                        <div className="header_bottom-adressText--adress">Rentrez votre adresse</div>
                    </div>
                </button>
            </div>

            <div className="header_bottom-options">
               <div> <a href="www.amazon.com" className="header_bottom-option">Meilleurs Ventes</a></div>
               <div> <a href="www.amazon.com" className="header_bottom-option">AmazonBasics</a></div>
               <div> <a href="www.amazon.com" className="header_bottom-option">Dernières Nouveautés</a></div>
               <div> <a href="www.amazon.com" className="header_bottom-option">Hight Tech</a></div>
               <div> <a href="www.amazon.com" className="header_bottom-option">Livres</a></div>
               <div> <a href="www.amazon.com" className="header_bottom-option">Service Client</a></div>
               <div> <a href="www.amazon.com" className="header_bottom-option">Cusine et Maison</a></div>
               <div> <a href="www.amazon.com" className="header_bottom-option">Idées Cadeaux</a></div>
               <div> <a href="www.amazon.com" className="header_bottom-option">Informatique</a></div>
               <div> <a href="www.amazon.com" className="header_bottom-option">Chèque Cadeaux</a></div>
               <div> <a href="www.amazon.com" className="header_bottom-option">Vendre</a></div>
               <div> <a href="www.amazon.com" className="header_bottom-option">Guide de l'acheteur</a></div>
               <div> <a href="www.amazon.com" className="header_bottom-option">Livraison Gratuite</a></div>

            </div>






            
        </div>
    )
}

export default Header_bottom
