import React from 'react';
import '../assets/css/Header.css'
import Header_top from './Header_top'
import Header_bottom from './Header_bottom' 

function Header() {
    return (
        <div className="header">
            <Header_top />
            <Header_bottom />           
        </div>
    )
}

export default Header
