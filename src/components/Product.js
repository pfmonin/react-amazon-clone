import React from 'react'
import '../assets/css/Product.css';


function Product() {
    return (
        <div className="product">
            <div className="product__infos">
                <p className="product-title"> KLAISER - Robot Pétrin Pâtisser Multifonctions KITCHEN MIX KM284MEX PROFESSIONAL </p>
                <p className="product__price">
                    <small>$</small>
                    <strong>45</strong>
                </p>
                <div className="product__rating">
                    <p>⭐</p>
                    <p>⭐</p>
                    <p>⭐</p>
                </div> 
            </div>

            <img className="product__image" src="https://www.media-rdc.com/medias/683aff7374493eeaadfc3c2b1b5f68eb/p_580x580/81yvq-2bvjgl-sl1500.jpg" alt=""/>
            
            <button className="product__button">Add to basket</button>
            
        </div>
    )
}

export default Product
