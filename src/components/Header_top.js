import React from 'react'
import '../assets/css/Header_top.css';
import SearchIcon from '@material-ui/icons/Search';
import ShoppingBasketIcon from '@material-ui/icons/ShoppingBasket';

function Header_top() {
    return (
        <div className="header_top">
            <div className="header_top-image">
                <img src="https://pngimg.com/uploads/amazon/amazon_PNG11.png" alt=""/>
            </div>
            
            <div className="header_top-Searchbar">
                <input type="text" className="header_top-SearchInput" />
                <div className="header_top-SearchIcon">
                    <button>
                        <SearchIcon style={{ color: "black" }} />
                    </button>
                </div>
            </div>

            <div className="header_top-options">
                <div className="header_top-option-signIn">
                    Bonjour, identifiez-vous
                    <span>Compte et listes</span>
                </div>
                <div className="header_top-option-orders">
                    Retours 
                    <span>et Commandes</span>
                </div>
                <div className="header_top-option-prime">
                    Testez
                    <span>Prime</span>
                </div>
                <div className="header_top-option-cart">
                    <ShoppingBasketIcon style={{color:"white"}} />
                    <span className="header__optionLineTwo header__basketCount">0</span>
                </div>                

            </div>
            
        </div>
    )
}

export default Header_top
